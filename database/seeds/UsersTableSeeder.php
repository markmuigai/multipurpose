<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a user
        User::create([
            'name' => 'Mark Muigai',
            'email' => 'mark@gmail.com',
            'password' => Hash::make('secret'),
            'type' => 'admin',
            'bio' => 'legend of awesomeness',
            'photo' => 'profile.png' 
        ]);
    }
}
