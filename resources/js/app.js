/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// Import Vue framework
window.Vue = require('vue');

// Import vue form validation
import { Form, HasError, AlertError } from 'vform';

// Import moment
import moment from 'moment';

// Import vue js progress bar
import VueProgressBar from 'vue-progressbar';

// Import sweet alert
import Swal from 'sweetalert2';

// Make swal availabe globally
window.Swal = Swal;

// Vue-progress bar parameters
const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
  // transition: {
  //   speed: '0.2s',
  //   opacity: '0.6s',
  //   termination: 300
  // },
  // autoRevert: true,
  // inverse: false
}

// Pass parameters to progress bar
Vue.use(VueProgressBar, options)

// Register validation components globally
window.Form = Form;  
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

// Vue Router
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Set Application routes
let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default }
  ]

// Register routes
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

  // Register filters
  Vue.filter('capitalize', function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1)
  })

  // Moment JS Filter
  Vue.filter('myDate', function(date) {
    return moment(date).format('MMMM Do YYYY, h:mm:ss a')
  })

  // Setup sweet alert toast function
  const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  // Make toast available gloabally
  window.Toast = toast;

  /**
   * Component communication
   * Let fire be a new instance of Vue js to provide access to methods emit and on
   * Global event listener object 
   */
  window.Fire = new Vue();;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
